/*	Name: Andrew Virginillo and Brandon Miller
	Date: October, 2015
	File: Assignment2.java
	
	Description: Java program that takes a users prescription and outputs their total cost based on choice
	of sunglasses or not and color.
	
	
	Analysis:
	
		Funtionality: The user will input his OS and OD prescription values,number of glasses they are buying,the designer,whether or not they are green or blue. the program will
		calculate your total prescription, and the cost of each pair and the total cost.
	
	---------------------------------------------------------
	TEST DATA:
		Inputs:		Outputs:	
		4.25		(4.25 + 2.5 + 4.0 + 1.5) = 12.25 
		2.5			($150.00 * 1.15) = $172.50
		4.0			($250.00 * 1.15) + $10.00 = $287.50
		1.5			$172.50 + $297.50 = $460.00
	----------------------------------------------------------
	
		Inputs:					Outputs:				Constants:
		osCyl real 2dp			subCost real 2dp		OSCAR = 150.00 2dp
		osSpher real 2dp		totalPresc real 2dp		FRANKLIN = 250.00 2dp 
		numGlasses int			designer string			GR_SUN = 10.00 2dp
		designer string			osCyl real 2dp			BR_SUN = 15.00 2dp
		subCost real 2dp		osSpher real 2dp		OVERAGE = 1.15 2dp
		typeGlasses boolean		odCyl real 2dp			D_SIGN = "$" string
		colorGlasses char		odSpher real 2dp		PRESC_THRESH = 10.00
		odCyl real 2dp			glassesOver real 2dp
		odSpher real 2dp		totalCounter real 2dp
		filename				
								
		Fomulas:
		subCost = FRANKLIN
		subCost = OSCAR
		totalPresc = osCyl + osSpher + odCyl +odSpher
		glassesOver = FRANKLIN * OVERAGE
		glassesOver = OSCAR * OVERAGE
		glassesNoOver = OSCAR
		glassesNoOver = FRANKLIN
		totalCounter += subCost
		

*/

import java.io.*;
import java.util.*;
import javax.swing.JOptionPane;

public class assignment2
{
	static final double OSCAR = 150.00;
	static final double FRANKLIN = 250.00;
	static final double GR_SUN = 10.00;
	static final double BR_SUN = 15.00;
	static final double OVERAGE = 1.15;
	public static final String D_SIGN = "$";
	static final double PRESC_THRESH = 10.00;
		
	public static void main (String[] args)
	{
		//declare and initialize all variables/strings
		Scanner sc = new Scanner(System.in);
		double osCyl;
		double osSpher;
		int numGlasses;
		double subCost = 0;
		Boolean typeGlasses;
		char colorGlasses;
		String designer="";
		String desginer1="";
		String designer2="";
		double odCyl;
		double odSpher;
		double totalPresc;
		double glassesOver;
		double totalCounter = 0;
		double glassesNoOver;
		String filename;
		int counter = 1;
		
		//filename, date & name
		filename = JOptionPane.showInputDialog("Please enter the path and filename of the report: ");
		UtilityClass outfile =  new UtilityClass(filename);
		outfile.openFile();
		String name = outfile.myName();
		outfile.writeLineToFile(name);
		System.out.println(name);
		String date = outfile.myDate();
		outfile.writeLineToFile(date);
		System.out.println(date);
		
		// prompt and get OS,OD and num Glasses
		System.out.println("Please enter the oculus dextrus spherical: ");
		odSpher = sc.nextDouble();
		System.out.println("Please enter the oculus dextrus cylinder: ");
		odCyl = sc.nextDouble();
		System.out.println("Please enter the oculus sinister spherical: ");
		osSpher = sc.nextDouble();
		System.out.println("Please enter the oculus sinister cylinder: ");
		osCyl = sc.nextDouble();
		System.out.println("Please enter the number of glasses being purchased today: ");
		numGlasses = sc.nextInt();
		totalPresc = odCyl+odSpher+osCyl+osSpher;
		
		
		//begin loop
		while (counter<=numGlasses)
		{
			designer = JOptionPane.showInputDialog("Please enter the designer of the glasses you are buying: \n").toUpperCase();
			//substrings
			desginer1=designer.substring(0,1).toUpperCase();
			designer2=designer.substring(1).toLowerCase();
			designer=desginer1+designer2;
			
			if (designer.equals("Oscar"))
			{
				if (totalPresc>PRESC_THRESH)
				{
					subCost = OSCAR * OVERAGE;
					System.out.printf("\nSubtotal of Oscar prescription glasses is %.2f\n", subCost);
					outfile.writeLineToFile("\nSubtotal of Oscar prescription glasses is %.2f\n", subCost);
					
				}
				else
				{
					subCost = OSCAR;
					System.out.printf("Subtotal of Oscar prescription glasses is %.2f\n", subCost);
					outfile.writeLineToFile("Subtotal of Oscar prescription glasses is %.2f\n", subCost);
					
				}
			}
			else if (designer.equals("Franklin"))
			{
				if (totalPresc > PRESC_THRESH)
				{
					subCost = FRANKLIN * OVERAGE;
					System.out.printf("Subtotal of Franklin prescription glasses is %.2f\n", subCost);
					outfile.writeLineToFile("Subtotal of Franklin prescription glasses is %.2f\n", subCost);
					
				}
				else 
				{
					subCost = FRANKLIN;
					System.out.printf("\nSubtotal of Franklin prescription glasses is %.2f\n", subCost);
					outfile.writeLineToFile("\nSubtotal of Franklin prescription glasses is %.2f\n", subCost);
					
				}
			}
			else 
			{
				System.out.printf("\n!ERROR!You have entered an invalid designer\n");
				continue; //if designer is not 'O' or 'F' will return to top of loop and prompt again
			}
			
			System.out.printf("\nAre the glasses you are buying sunglasses true or false:\n");
			typeGlasses = sc.nextBoolean();
			if (typeGlasses == true)
			{
				System.out.printf("Total prescription is %.2f\n", totalPresc);
				System.out.printf("Please enter the color of the sunglasses G/B: ");
				outfile.writeLineToFile("\nTotal prescription is %.2f\n", totalPresc);
				colorGlasses = sc.next().toUpperCase().charAt(0);
				switch (colorGlasses)
				{
					case 'G':
					{
						subCost+=GR_SUN;
						break;
					}
					case 'B':
					{
						subCost+=BR_SUN;
						break;
					}
					default:
					{
						System.out.printf("\n!ERROR! Invalid color\n");
						continue;
					}
				}
			}	
			else 
			{
				System.out.printf("\nTotal prescription is %.2f\n", totalPresc);
				outfile.writeLineToFile("\nTotal prescription is %.2f\n", totalPresc);
			}
			System.out.printf("\n%s\t %.2f\t %.2f\t %.2f\t %.2f\t $%.2f\n\n\n",designer, odCyl, odSpher, osCyl, osSpher, subCost);
			outfile.writeLineToFile("\n%s\t %.2f\t %.2f\t %.2f\t %.2f\t $%.2f\n\n\n",designer, odCyl, odSpher, osCyl, osSpher, subCost);
			totalCounter+=subCost;
			counter++;
		}
		if(numGlasses==1) //grammar for ammount of glasses loop
		{
			System.out.printf("\nThe cost of your %d pair of glasses is $%.2f", numGlasses, totalCounter);
			outfile.writeLineToFile("\nThe cost of your %d pair of glasses is $%.2f", numGlasses, totalCounter);
			System.out.print("\ngoodbye!\n");
			outfile.writeLineToFile("\ngoodbye!\n");
		}
		else
		{
			System.out.printf("The cost of your %d pairs of glasses is $%.2f", numGlasses, totalCounter);
			outfile.writeLineToFile("The cost of your %d pairs of glasses is $%.2f", numGlasses, totalCounter);
			System.out.print("\ngoodbye!\n");
			outfile.writeLineToFile("\ngoodbye!\n");
		}
	}//end main
	
}//end assignment 2


		
		